#!/bin/bash
#git clone https://github.com/LAfricain/Menge-Bibel.git
cd Menge-Bibel
rm -r usfm
mkdir usfm
cp -r Bibel/*/*.md usfm/
cd usfm/
rm 06\ -\ Brief\ Jeremias.md

rename 's/ //g' *md
###Cette ligne remplace le nom du livre par son id et le chap 1.
sed -ri 's/# (ERSTES BUCH DER CHRONIK)/\\id 1CH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Chr\n\\mt \1/g' *.md
sed -ri 's/# (DER ERSTE BRIEF DES APOSTELS PAULUS AN DIE KORINTHER)/\\id 1CO\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Kor\n\\mt \1/g' *.md
sed -ri 's/# (DER ERSTE BRIEF DES JOHANNES)/\\id 1JN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Jo\n\\mt \1/g' *.md
sed -ri 's/# (DAS ERSTE BUCH DER KÖNIGE)/\\id 1KI\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Kön\n\\mt \1/g' *.md
sed -ri 's/# (DER ERSTE BRIEF DES APOSTELS PETRUS)/\\id 1PE\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Petr\n\\mt \1/g' *.md
sed -ri 's/# (DAS ERSTE BUCH SAMUEL)/\\id 1SA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Sam\n\\mt \1/g' *.md
sed -ri 's/# (DER ERSTE BRIEF DES APOSTELS PAULUS AN DIE THESSALONIKER)/\\id 1TH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Thess\n\\mt \1/g' *.md
sed -ri 's/# (DER ERSTE BRIEF DES APOSTELS PAULUS AN TIMOTHEUS)/\\id 1TI\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Tim\n\\mt \1/g' *.md
sed -ri 's/# (ZWEITES BUCH DER CHRONIK)/\\id 2CH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Chr\n\\mt \1/g' *.md
sed -ri 's/# (DER ZWEITE BRIEF DES APOSTELS PAULUS AN DIE KORINTHER)/\\id 2CO\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Kor\n\\mt \1/g' *.md
sed -ri 's/# (DER ZWEITE BRIEF DES JOHANNES)/\\id 2JN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Jo\n\\mt \1/g' *.md
sed -ri 's/# (DAS ZWEITE BUCH DER KÖNIGE)/\\id 2KI\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Kön\n\\mt \1/g' *.md
sed -ri 's/# (DER ZWEITE BRIEF DES APOSTELS PETRUS)/\\id 2PE\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Petr\n\\mt \1/g' *.md
sed -ri 's/# (DAS ZWEITE BUCH SAMUEL)/\\id 2SA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Sam\n\\mt \1/g' *.md
sed -ri 's/# (DER ZWEITE BRIEF DES APOSTELS PAULUS AN DIE THESSALONIKER)/\\id 2TH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Thess\n\\mt \1/g' *.md
sed -ri 's/# (DER ZWEITE BRIEF DES APOSTELS PAULUS AN TIMOTHEUS)/\\id 2TI\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Tim\n\\mt \1/g' *.md
sed -ri 's/# (DER DRITTE BRIEF DES JOHANNES)/\\id 3JN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 3\.Jo\n\\mt \1/g' *.md
sed -ri 's/# (DIE APOSTELGESCHICHTE)/\\id ACT\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Apg\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET AMOS)/\\id AMO\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Am\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN DIE KOLOSSER)/\\id COL\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Kol\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET DANIEL)/\\id DAN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Dan\n\\mt \1/g' *.md
sed -ri 's/# (DAS FÜNFTE BUCH MOSE)/\\id DEU\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 5\.Mose\n\\mt \1/g' *.md
sed -ri 's/# (DER PREDIGER)/\\id ECC\n\\h\n\\toc1 \1\n\\toc2\n\\toc3\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN DIE EPHESER)/\\id EPH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Eph\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH ESTHER)/\\id EST\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Est\n\\mt \1/g' *.md
sed -ri 's/# (DAS ZWEITE BUCH MOSE)/\\id EXO\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Mose\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET HESEKIEL \(EZECHIEL\))/\\id EZK\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hes\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH ESRA)/\\id EZR\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Esr\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN DIE GALATER)/\\id GAL\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Gal\n\\mt \1/g' *.md
sed -ri 's/# (DAS ERSTE BUCH MOSE)/\\id GEN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Mose\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET HABAKUK)/\\id HAB\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hab\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET HAGGAI)/\\id HAG\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hag\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF AN DIE HEBRÄER)/\\id HEB\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hebr\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET HOSEA)/\\id HOS\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hos\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET JESAJA)/\\id ISA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jes\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES JAKOBUS)/\\id JAS\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jak\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH DER RICHTER)/\\id JDG\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Ri\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET JEREMIA)/\\id JER\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jer\n\\mt \1/g' *.md
sed -ri 's/# (DIE HEILSBOTSCHAFT NACH JOHANNES)/\\id JHN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Joh\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH HIOB)/\\id JOB\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hiob\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET JOEL)/\\id JOL\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Joel\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET JONA)/\\id JON\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jona\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH JOSUA)/\\id JOS\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jos\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES JUDAS)/\\id JUD\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Jude\n\\mt \1/g' *.md
sed -ri 's/# (DIE KLAGELIEDER)/\\id LAM\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Kla\n\\mt \1/g' *.md
sed -ri 's/# (DAS DRITTE BUCH MOSE)/\\id LEV\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 3\.Mose\n\\mt \1/g' *.md
sed -ri 's/# (DIE HEILSBOTSCHAFT NACH LUKAS)/\\id LUK\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Lk\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET MALEACHI)/\\id MAL\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Mal\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET MICHA)/\\id MIC\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Mi\n\\mt \1/g' *.md
sed -ri 's/# (DIE HEILSBOTSCHAFT NACH MARKUS)/\\id MRK\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Mk\n\\mt \1/g' *.md
sed -ri 's/# (DIE HEILSBOTSCHAFT NACH MATTHÄUS)/\\id MAT\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Mt\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET NAHUM)/\\id NAM\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Nah\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH NEHEMIA)/\\id NEH\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Neh\n\\mt \1/g' *.md
sed -ri 's/# (DAS VIERTE BUCH MOSE)/\\id NUM\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 4\.Mose\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET OBADJA)/\\id OBA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Oba\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN PHILEMON)/\\id PHM\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Phlm\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN DIE PHILIPPER)/\\id PHP\n\\h\n\\toc1 \1\n\\toc2 \n\\toc3 Phil\n\\mt \1/g' *.md
sed -ri 's/# (DIE SPRÜCHE)/\\id PRO\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Spr\n\\mt \1/g' *.md
sed -ri 's/# (DIE PSALMEN)/\\id PSA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Ps\n\\mt \1/g' *.md
sed -ri 's/# (DIE OFFENBARUNG DES JOHANNES)/\\id REV\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Offb\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN DIE RÖMER)/\\id ROM\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Röm\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH RUTH)/\\id RUT\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Ruth\n\\mt \1/g' *.md
sed -ri 's/# (DAS HOHELIED)/\\id SNG\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Hoh\n\\mt \1/g' *.md
sed -ri 's/# (DER BRIEF DES APOSTELS PAULUS AN TITUS)/\\id TIT\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Tit\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET SACHARJA)/\\id ZEC\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Sach\n\\mt \1/g' *.md
sed -ri 's/# (DER PROPHET ZEPHANJA)/\\id ZEP\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Zep\n\\mt \1/g' *.md
sed -ri 's/# (JUDITH)/\\id JDT\n\\h\n\\toc1 \1\n\\toc2\n\\toc3\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH DER WEISHEIT \(oder Die Weisheit Salomos\))/\\id WIS\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Wei\n\\mt \1/g' *.md
sed -ri 's/# (TOBIT UND TOBIAS)/\\id TOB\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Tob\n\\mt \1/g' *.md
sed -ri 's/# (DIE WEISHEIT JESU, DES SOHNES SIRACHS)/\\id SIR\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Sir\n\\mt \1/g' *.md
sed -ri 's/# (DAS BUCH BARUCH)/\\id BAR\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 Bar\n\\mt \1/g' *.md
sed -ri 's/# (DAS ERSTE BUCH DER MAKKABÄER)/\\id 1MA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 1\.Makk\n\\mt \1/g' *.md
sed -ri 's/# (DAS ZWEITE BUCH DER MAKKABÄER)/\\id 2MA\n\\h\n\\toc1 \1\n\\toc2\n\\toc3 2\.Makk\n\\mt \1/g' *.md
sed -ri 's/# (ZUSÄTZE ZUM BUCH DANIEL)/\\id DAG\n\\h\n\\toc1 \1\n\\toc2\n\\toc3\n\\mt \1/g' *.md
sed -ri 's/# (DAS GEBET MANASSES)/\\id MAN\n\\h\n\\toc1 \1\n\\toc2\n\\toc3\n\\mt \1/g' *.md
sed -ri 's/# (ZUSÄTZE ZUM BUCH ESTER)/\\id ESG\n\\h\n\\toc1 \1\n\\toc2 \n\\toc3\n\\mt \1/g' *.md
##Chapitres
sed -ri 's/__([0-9]*)__/\\c \1\n\\p\n/g' *.md
#__1__
#verset
sed -ri 's/^<sup>([0-9]*)<\/sup>/\\v \1 /g' *.md

#Titre de Psaumes
sed -ri 's/(\\v [1-3] )<em>(.*)<\/em>/\\d \1\2/g' *.md
sed -ri 's/<em>|<\/em>//g' *.md
sed -ri 's/\* \* \*//g' *.md
#citation
sed -ri ':a;N;$!ba;s/<blockquote>\n<blockquote>/<blockquote>/'g *.md
sed -ri ':a;N;$!ba;s/<\/blockquote>\n<\/blockquote>/<\/blockquote>/g' *.md
sed -ri 's/<blockquote>|<\/blockquote>/\\q /g' *.md
sed -ri 's/<ul>|<\/ul>/\\q /g' *.md
sed -ri 's/<li>|<\/li>//g' *.md
sed -ri ':a;N;$!ba;s/\\q\n\\q/\\q/'g *.md
sed -ri ':a;N;$!ba;s/(Machthaber)\n(auf Erden)/\1 \2/g' *md
## SELA.
sed -ri 's/SELA\./\\qs SELA\.\\qs\*/g' *.md
##Titres
sed -ri 's/^## (.*)$/\\s1 \1\n\\p\n/g' *.md
sed -ri 's/^### (.*)$/\\s2 \1\n\\p\n/g' *.md
sed -ri 's/^#### (.*)$/\\s3 \1\n\\p\n/g' *.md
sed -ri 's/^##### (.*)$/\\s4 \1\n\\p\n/g' *.md
sed -ri '/^$/d' *.md
#refcroisées
sed -ri 's/<sup title="([A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*|[A-Z1-5][A-Za-z]* [0-9]*,[0-9]*-[0-9]*|[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*;[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*|[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*; [0-9]*,[0-9]*-[0-9]*)">\&\#x2732;<\/sup>/\\x \+ \\xt \1\\x\*/g' *.md
#notes
sed -ri 's/<sup title="/\\f \+ \\ft /g' *.md
sed -ri 's/<span class="auslegung">(.*)<\/span>/\\f \+ \\ft \1\\f\*/g' *.md
sed -ri 's/">\&\#x2732;<\/sup>/\\f\*/g' *.md

#Refcroisées
#sed -ri 's/\\f \+ \\ft ([A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*|[A-Z1-5][A-Za-z]* [0-9]*,[0-9]*-[0-9]*|[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*;[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*|[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*; [0-9]*,[0-9]*-[0-9]*|[A-Z1-5][.A-Za-z]* [0-9]*,[0-9]*.*[0-9])\\f\*/\\x \+ \\xt \1\\x\*/g' *.md
sed -ri 's/(\\f \+ \\ft vgl\. )(.*\\f\*)/\1\\fr \2/g' *md

##Estra characters
sed -ri 's/<span data-param="*.*" class="fussnote">[A-Z]<\/span>|<\/span>//g' *.md
sed -ri 's/\\f \+ \\ft <a class="bibelstelle" href="\/bibeln\/online-bibeln\/menge-bibel\/bibeltext\/bibel\/text\/lesen\/stelle\/1\/120003\/120003\/">(Gen 12,3)<\/a><a class="bibelstelle" href="\/bibeln\/online-bibeln\/menge-bibel\/bibeltext\/bibel\/text\/lesen\/stelle\/1\/180018\/180018\/">(18,18)<\/a>\\f\*/\\x \+ \\xt \1 \2\\x\*/g' *.md
##suppression extra tag
sed -ri ':a;N;$!ba;s/\\p\n\\s/\\s/g' *.md
sed -ri ':a;N;$!ba;s/\\p\n\\d/\\q\n\\d/g' *.md
sed -ri ':a;N;$!ba;s/(\\s2 6\. K.*zurück)\n\\p\n(\\f \+ \\ft anstatt Esther 8,13\\f\*)\n\\c 6/\\c 1\n\\p\n\\v 1 \[\]\n\\c 6\n\1\2\n/g' 09-ZusätzeEster.md
##Déplace les titres qui sont avant les chapitres
#for i in {1..3}; do titre-chap.sh; done
for f in *md; do ./titre-chap.py "$f" "$f"nv; done
rename 's/mdnv/usfm/g' *mdnv
rm *md
cd ../../
u2o.py -e utf-8 -l de -o germenge.osis -v -d -x GerMenge Menge-Bibel/usfm/*.usfm

##Résoudre prob addDan et addEsth
sed -ri 's/(sID=")EsthGr/\1AddEsth/g' germenge.osis
sed -ri 's/(sID=")DanGr/\1AddDan/g' germenge.osis
#orefs.py -o germenge.osis -i MengeRef -c create
orefs.py -i germenge.osis -o germenge.osis.xml -c MengeRef
xmllint --noout --schema ~/.bin/osisCore.2.1.1.xsd germenge.osis.xml 

mkdir ~/.sword/modules/texts/ztext/germenge/
osis2mod ~/.sword/modules/texts/ztext/germenge/ germenge.osis.xml -z -v Luther


