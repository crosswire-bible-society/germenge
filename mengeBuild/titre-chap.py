#! /usr/bin/python

import sys, re

bloc_spc=re.compile(r'^(\\s[0-3].*?\n)(\\p.*\n)?(\\c.*?\n)',re.M)

with open(sys.argv[1],'r') as f: fic=f.read()
with open(sys.argv[2],'w') as g:

   while True :
      k=bloc_spc.search(fic)
      if(k) :
         g.write(fic[:k.start(1)])
         g.write(k.group(3))
         g.write(k.group(1))
         fic=fic[k.end(3):]
      else :
         g.write(fic)
         break
         done
